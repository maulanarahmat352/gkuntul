﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LiftBehavior : MonoBehaviour
{
    public GameObject[] LiftDoor = new GameObject[2];
    public Vector3[] ClosedDoor = new Vector3[2];
    public Vector3[]OpenDoor = new Vector3[2]; 
    public void OpenLift()
    {
        LiftDoor[0].transform.DOMove(OpenDoor[0], 1f, false);
        LiftDoor[1].transform.DOMove(OpenDoor[1], 1f, false);
    }

    public void CloseLift()
    {
        LiftDoor[0].transform.DOMove(ClosedDoor[0], 0.5f, false);
        LiftDoor[1].transform.DOMove(ClosedDoor[1], 0.5f, false);
    }

    public void Start()
    {
        for(int i =0; i < LiftDoor.Length; i++)
        {
            ClosedDoor[i] = LiftDoor[i].transform.position;
        }
        OpenDoor[0] = ClosedDoor[0];
        OpenDoor[1] = ClosedDoor[1];

        OpenDoor[0].z = ClosedDoor[0].z + 7;
        OpenDoor[1].z = ClosedDoor[1].z - 7;

       // OpenLift();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            OpenLift();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            CloseLift();
        }
    }

}
