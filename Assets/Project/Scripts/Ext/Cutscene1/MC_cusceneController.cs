﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MC_cusceneController : MonoBehaviour
{
    [SerializeField] GameObject Player;
    Animator anim;
    public bool walk=false, react = false, sit = false, typing = false;
    // Start is called before the first frame update
    void Start()
    {
        anim = Player.GetComponent<Animator>();
       // anim.Play("Idle");
    }

    // Update is called once per frame
    void Update()
    {
        if (walk)
        {
            anim.Play("walking");
            walk = false;
        }
        else if (react)
        {
            anim.Play("");
            react = false;
        }else if (sit)
        {
            anim.Play("");
            sit = false;
        }
        else if (typing)
        {
            anim.Play("");
            typing = false;
        }
            

    }
}
