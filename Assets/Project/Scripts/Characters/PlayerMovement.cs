﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private AudioClip[] m_FootstepSounds;
    private AudioSource m_AudioSource;
    private float m_StepCycle =0.25f;
    private float m_NextStep = 0.25f;
    bool loud=false;
    public float RunMultiply = 10;
    float RunBeff;
    public float RunSoundMultiply =1;
    [SerializeField]GameObject Player;
    Animator anim;
    private float heading;
    private Vector2 input;
    const float constantSpeed = 1f;
    const float mouseSensitivity = 100f;
    [SerializeField]private Camera cam;
    [SerializeField] Transform CamPivot;
    bool move = false;
    // Start is called before the first frame update
    void Start()
    {
        RunBeff = RunMultiply;
        anim = Player.GetComponent<Animator>();
        cam = Camera.main;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementCharacter();
        rotateThePlayer();
    }
    void rotateThePlayer()
    {
        if (move == true)
        {
            if (cam != null)
            {
                Vector3 camRot = new Vector3(0, cam.transform.eulerAngles.y, 0);
                transform.rotation = Quaternion.Euler(camRot);

            }
        }
    }

    void MovementCharacter()
    {

        float horizontal = Input.GetAxis("Horizontal") * RunMultiply;
        float vertical = Input.GetAxis("Vertical") * RunMultiply;

        heading += Input.GetAxis("Mouse X") * Time.deltaTime * 180;
        CamPivot.rotation = Quaternion.Euler(0, heading, 0);

        input = new Vector2(horizontal, vertical);
        input = Vector2.ClampMagnitude(input, 1);
        
        if(cam != null)
        {
            Vector3 camF = cam.gameObject.transform.forward;
            Vector3 camR = cam.gameObject.transform.right;

            camF.y = 0;
            camR.y = 0;
            camF = camF.normalized;
            camR = camR.normalized;
            transform.position += (camF * input.y + camR * input.x) * Time.deltaTime * RunMultiply;

        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            RunMultiply = RunBeff*2;
            RunSoundMultiply = 1.2f;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            RunSoundMultiply = 1;
            RunMultiply = RunBeff;
        }

        anim.SetFloat("VelocityX", horizontal/RunBeff);
        anim.SetFloat("VelocityZ", vertical/RunBeff);

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            anim.SetBool("Move", true);
            move = true;
            
          
                m_NextStep -= Time.deltaTime;
                if (m_NextStep <= 0)
                {
                    if(!loud)
                    PlayFootStepAudio();
                loud = true;

                    m_StepCycle -= Time.deltaTime;
                    if (m_StepCycle <=0)
                    {
                        m_NextStep = 0.25f/RunSoundMultiply;
                        loud = false;
                     m_StepCycle = 0.25f/RunSoundMultiply;
                    }
                }
          
            
        }
        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            anim.SetBool("Move", false);
            move = false;
        }
    }
    private void PlayFootStepAudio()
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, m_FootstepSounds.Length);
        m_AudioSource.clip = m_FootstepSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        m_FootstepSounds[n] = m_FootstepSounds[0];
        m_FootstepSounds[0] = m_AudioSource.clip;
    }
}
