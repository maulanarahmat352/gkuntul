﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDynamic : MonoBehaviour
{

    [SerializeField] Transform Cam;
    [SerializeField] Transform Player;
    [SerializeField] Transform Crossair;
    float mouseSesitivity= 0.00005f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X")*mouseSesitivity*Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y")*mouseSesitivity*Time.deltaTime;

        Cam.RotateAround(Player.position, Vector3.up, 1);
        Crossair.RotateAround(Player.position, Vector3.up, 1);
    }
}
