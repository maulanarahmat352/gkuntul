﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacter : MonoBehaviour
{

    [SerializeField]
    private float speed;
    [SerializeField]
    private float gravity = 20.0f;
    [SerializeField]
    private float camSensitivity = 1.5f;

    private Vector3 direction;

    private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        CameraController();
        MovementCharacter();
    }

    void MovementCharacter()
    {
        //Move Kanan Kiri Atas Bawah
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        direction = new Vector3(horizontal, 0, vertical);
        direction *= speed;

        transform.Translate(direction * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = 3.5f;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = 6f;
        }
    }

    void CameraController()
    {
        //Camera On Mouse
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        //Kiri Kanan Look
        Vector3 rotationY = transform.localEulerAngles;
        rotationY.y += mouseX * camSensitivity;
        transform.localRotation = Quaternion.AngleAxis(rotationY.y, Vector3.up);

        //Atas Bawah Look
        Vector3 rotationX = cam.gameObject.transform.localEulerAngles;
        rotationX.x -= mouseY * camSensitivity;
        cam.gameObject.transform.localRotation = Quaternion.AngleAxis(rotationX.x, Vector3.right);
    }
}
