﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;

public class Cutscene1_Post : MonoBehaviour
{
    private PostProcessVolume postProcessVolume;
    // Start is called before the first frame update
    void Start()
    {
        postProcessVolume = GetComponent<PostProcessVolume>();
        StartCoroutine(StartFocusing(0.1f,10.0f,9f));

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator StartFocusing(float v_start, float v_end, float duration)
    {
        DepthOfField pr;
        pr = postProcessVolume.sharedProfile.GetSetting<DepthOfField>();
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            pr.focusDistance.value = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
           // Debug.Log(pr.focusDistance.value);
            yield return null;
        }
        pr.focusDistance.value = v_end;
    }
}
