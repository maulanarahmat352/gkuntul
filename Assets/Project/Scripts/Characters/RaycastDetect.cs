﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastDetect : MonoBehaviour
{
    public LayerMask layerMask;
    [SerializeField] GameObject Interact;
    GameObject SelectionObject;
    string ObjectRayCasted;
    public string[] obj =  new string[] { "lift","" };

private void Start()
    {
        Interact.SetActive(false);
    }
    void Update()
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
            {
                if (hit.collider != null)
                {
                    ObjectRayCasted = hit.collider.gameObject.name;
                    SelectionObject = GameObject.Find(ObjectRayCasted);
                    float dist = hit.distance;
                    if (dist <= 1f)
                    {
                        Interact.SetActive(true);
                    }
                    else if (dist > 1f)
                    {
                        Interact.SetActive(false);
                    }
                }

                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                Debug.Log("Hit");
            }
            else
            {
                Interact.SetActive(false);
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Not Hit");
            }
    }
}