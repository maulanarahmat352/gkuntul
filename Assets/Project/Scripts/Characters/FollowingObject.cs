﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingObject : MonoBehaviour
{
    public Transform playerPos;
    public Vector3 ObjOffset;
    public float smoothFac;
    public bool lookAtPlayer = false;

    // Start is called before the first frame update
    void Start()
    {
        ObjOffset = transform.position - playerPos.position;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        Vector3 newPos = playerPos.transform.position + ObjOffset;
        transform.position = Vector3.Slerp(transform.position, newPos, smoothFac);
        if (lookAtPlayer)
            transform.LookAt(playerPos);
    }
}
